package com.yan.stretchbitmap;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView mYuanTuView;
    // 拉伸
    private ImageView mStretchView;
    private EditText mStretchXView;
    private EditText mStretchYView;
    private EditText mStretchWidthView;
    private EditText mStretchHeightView;
    private Button mButton1;
    // 平铺
    private ImageView mTileView;
    private EditText mTileX1View;
    private EditText mTileX2View;
    private EditText mTileY1View;
    private EditText mTileY2View;
    private EditText mTileWidthView;
    private EditText mTileHeightView;
    private Button mButton2;

    private Bitmap mBitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.test);

        findView();

    }

    private void findView() {
        mYuanTuView = findViewById(R.id.imv_yuantu);
        mYuanTuView.setImageBitmap(mBitmap);

        mStretchXView = findViewById(R.id.et_x);
        mStretchYView = findViewById(R.id.et_y);
        mStretchWidthView = findViewById(R.id.et_width1);
        mStretchHeightView = findViewById(R.id.et_height1);
        Button button1 = findViewById(R.id.btn_1);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shownStretchBitmap();
            }
        });
        mStretchView = findViewById(R.id.imv_stretch);

        mTileX1View = findViewById(R.id.et_x1);
        mTileX2View = findViewById(R.id.et_x2);
        mTileY1View = findViewById(R.id.et_y1);
        mTileY2View = findViewById(R.id.et_y2);
        mTileWidthView = findViewById(R.id.et_width2);
        mTileHeightView = findViewById(R.id.et_height2);
        Button button2 = findViewById(R.id.btn_2);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shownTileBitmap();
            }
        });
        mTileView = findViewById(R.id.imv_tile);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_1:
                shownStretchBitmap();
                break;
            case R.id.btn_2:
                shownTileBitmap();
                break;
            default:
                break;
        }
    }

    private void shownStretchBitmap() {
        String x = mStretchXView.getText().toString();
        double stretch_X = Double.valueOf(x);
        String y = mStretchYView.getText().toString();
        double stretch_Y = Double.valueOf(y);
        String w = mStretchWidthView.getText().toString();
        int width = Integer.valueOf(w);
        String h = mStretchHeightView.getText().toString();
        int height = Integer.valueOf(h);
        Bitmap bitmap = BitmapUtil.stretchBitmapByPoint(mBitmap, width, height, stretch_X, stretch_Y);
        mStretchView.setImageBitmap(bitmap);
    }

    private void shownTileBitmap() {
        String x1 = mTileX1View.getText().toString();
        double tile_X1 = Double.valueOf(x1);
        String x2 = mTileX2View.getText().toString();
        double tile_X2 = Double.valueOf(x2);
        String y1 = mTileY1View.getText().toString();
        double tile_Y1 = Double.valueOf(y1);
        String y2 = mTileY2View.getText().toString();
        double tile_Y2 = Double.valueOf(y2);
        String w = mTileWidthView.getText().toString();
        int width = Integer.valueOf(w);
        String h = mTileHeightView.getText().toString();
        int height = Integer.valueOf(h);
        Bitmap bitmap = BitmapUtil.tileBitmapByPoint(mBitmap, width, height, tile_X1, tile_X2, tile_Y1, tile_Y2);
        mTileView.setImageBitmap(bitmap);
    }

}
