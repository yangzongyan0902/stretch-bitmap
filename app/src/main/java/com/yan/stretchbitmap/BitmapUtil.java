package com.yan.stretchbitmap;

import android.graphics.Bitmap;
import android.graphics.Matrix;

/**
 * 图片拉伸、平铺的工具类
 * @author YangZongYan
 */
public class BitmapUtil {

    /**
     * 拉伸图片。指定横向拉伸的点和纵向拉伸的点，拉伸为.9图的效果
     *
     * @param bitmap    原图
     * @param width     新图片的宽度
     * @param height    新图片的高度
     * @param stretch_X 水平方向上被拉伸点的位置（该点到最左边的距离除以总宽度得到的值）
     * @param stretch_Y 垂直方向上被拉伸点的位置（该点到最上面的距离除以总高度得到的值）
     */
    public static Bitmap stretchBitmapByPoint(Bitmap bitmap, int width, int height, double stretch_X, double stretch_Y) {
        if (null == bitmap) {
            return null;
        }
        int bmpWidth = bitmap.getWidth();
        int bmpHeight = bitmap.getHeight();
        // 如果新图片宽或者高小于原图，计算出缩小比例
        if (width < bmpWidth || height < bmpHeight) {
            float scaleNum;
            float linshiWidth;
            float linshiHeight;
            // 计算放大比例，使得新图最小的边刚好等于原图对应的长度
            if (width / bmpWidth < height / bmpHeight) {
                scaleNum = (float) bmpWidth / width; // 放大倍数， 后面获取bitmap后需再缩小scaleNum倍
                linshiWidth = bmpWidth;  // 宽度放大为原图宽度
                linshiHeight = height * scaleNum; // 高度同比例增大
            } else {
                scaleNum = (float) bmpHeight / height;
                linshiWidth = width * scaleNum;
                linshiHeight = bmpHeight;
            }
            // 获取一个模拟的宽不变，纵向拉伸的临时图
            Bitmap bitmap1 = stretchBitmapByPoint(bitmap, (int) linshiWidth, (int) linshiHeight, stretch_X, stretch_Y);
            // 缩小临时图为最开始指定的大小
            Matrix matrix = new Matrix();
            float sx = 1f / scaleNum;
            matrix.postScale(sx, sx);
            return Bitmap.createBitmap(bitmap1, 0, 0, (int) linshiWidth, (int) linshiHeight, matrix, true);
        } else {
            int[] bmpPixels = new int[bmpWidth * bmpHeight];
            int[] newPixels = new int[width * height];
            bitmap.getPixels(bmpPixels, 0, bmpWidth, 0, 0, bmpWidth, bmpHeight);
            int[][] newPoints = new int[height][width];
            int point_x = (int) (stretch_X * bmpWidth); // 横线拉伸点的索引
            if (stretch_X == 1) {
                point_x = point_x - 1;
            }
            int point_y = (int) (stretch_Y * bmpHeight); // 垂直拉伸点的索引
            if (stretch_Y == 1) {
                point_y = point_y - 1;
            }
            int x1 = point_x;
            int x2 = width - (bmpWidth - point_x);
            int y1 = point_y;
            int y2 = height - (bmpHeight - point_y);
            for (int i = 0; i < height; ++i) {
                for (int j = 0; j < width; ++j) {
                    if (i < y1) {
                        if (j < x1) {                   // 左上角部分
                            newPoints[i][j] = bmpPixels[i * bmpWidth + j];
                        } else if (j < x2) { // 上边中间部分
                            newPoints[i][j] = bmpPixels[i * bmpWidth + x1];
                        } else {                        // 右上角区域
                            newPoints[i][j] = bmpPixels[i * bmpWidth + j - (width - bmpWidth)];
                        }
                    } else if (i < y2) {
                        if (j < x1) {                   // 左边中间部分
                            newPoints[i][j] = bmpPixels[point_y * bmpWidth + j];
                        } else if (j < x2) { // 中间部分
                            newPoints[i][j] = bmpPixels[bmpWidth * point_y + point_x];
                        } else {                         // 右边中间部分
                            newPoints[i][j] = bmpPixels[point_y * bmpWidth + j - (width - bmpWidth)];
                        }
                    } else {
                        int rowNum = i - (height - bmpHeight); // 当前行数
                        if (j < x1) {                   // 左下角部分
                            newPoints[i][j] = bmpPixels[rowNum * bmpWidth + j];
                        } else if (j < x2) { // 底部中间部分
                            newPoints[i][j] = bmpPixels[rowNum * bmpWidth + point_x];
                        } else {                        // 右下角部分
                            newPoints[i][j] = bmpPixels[rowNum * bmpWidth + j - (width - bmpWidth)];
                        }
                    }
                    newPixels[i * width + j] = newPoints[i][j];
                }
            }
            return Bitmap.createBitmap(newPixels, width, height, Bitmap.Config.ARGB_8888);
        }
    }

    /**
     * 平铺图片。指定图片上横向平铺区域的起始点和重点，达到
     *
     * @param bitmap 原图
     * @param width  新图片的宽度
     * @param height 新图片的高度
     * @param x1     水平方向被平铺区域的起点位置（该点到最左边的距离除以总宽度得到的值）
     * @param x2     水平方向被平铺区域的终点位置（该点到最左边的距离除以总宽度得到的值）
     * @param y1     垂直方向被平铺区域的起点位置（该点到最上边的距离除以总宽度得到的值）
     * @param y2     垂直方向被平铺区域的终点位置（该点到最上边的距离除以总宽度得到的值）
     */
    public static Bitmap tileBitmapByPoint(Bitmap bitmap, int width, int height, double x1, double x2, double y1, double y2) {
        if (null == bitmap) {
            return null;
        }
        // 没有可平铺区域，就返回原图
        if (x1 == x2 && y1 == y2) {
            return bitmap;
        }
        int bmpWidth = bitmap.getWidth();
        int bmpHeight = bitmap.getHeight();
        // 如果新图片宽或者高小于原图，计算出缩小比例
        if (width < bmpWidth || height < bmpHeight) {
            float scaleNum;
            float linshiWidth;
            float linshiHeight;
            // 计算放大比例，使得新图最小的边刚好等于原图对应的长度
            if (width / bmpWidth < height / bmpHeight) {
                scaleNum = (float) bmpWidth / width; // 放大倍数， 后面获取bitmap后需再缩小scaleNum倍
                linshiWidth = bmpWidth;  // 宽度放大为原图宽度
                linshiHeight = height * scaleNum; // 高度同比例增大
            } else {
                scaleNum = (float) bmpHeight / height;
                linshiWidth = width * scaleNum;
                linshiHeight = bmpHeight;
            }
            // 获取一个模拟的宽不变，纵向拉伸的临时图
            Bitmap bitmap1 = tileBitmapByPoint(bitmap, (int) linshiWidth, (int) linshiHeight, x1, x2, y1, y2);
            // 缩小临时图为最开始指定的大小
            Matrix matrix = new Matrix();
            float sx = 1f / scaleNum;
            matrix.postScale(sx, sx);
            return Bitmap.createBitmap(bitmap1, 0, 0, (int) linshiWidth, (int) linshiHeight, matrix, true);
        } else {
            // 获取旧图片的 pixels
            int[] bmpPixels = new int[bmpWidth * bmpHeight];
            bitmap.getPixels(bmpPixels, 0, bmpWidth, 0, 0, bmpWidth, bmpHeight);

            int[] newPixels = new int[width * height];
            int point_x1 = (int) (x1 * bmpWidth);  // 横向平铺区域的起始点的索引
            if (x1 == 1) {
                point_x1 -= 1;
            }
            int point_x2 = (int) (x2 * bmpWidth);  // 横向平铺区域的终点的索引
            if (x2 == 1) {
                point_x2 -= 1;
            }
            int copy_x = point_x2 - point_x1;      // 横向平埔区域的点的索引的范围
            int point_y1 = (int) (y1 * bmpHeight); // 垂直平铺区域的起始点的索引
            if (y1 == 1) {
                point_y1 -= 1;
            }
            int point_y2 = (int) (y2 * bmpHeight); // 垂直平铺区域的终点的索引
            if (y2 == 1) {
                point_y2 -= 1;
            }
            int copy_y = point_y2 - point_y1;      // 垂直平埔区域的点的索引的范围

            for (int i = 0; i < height; ++i) {
                for (int j = 0; j < width; ++j) {
                    if (i < point_y1) {
                        if (j < point_x1) {                   // 左上角部分
                            newPixels[i * width + j] = bmpPixels[i * bmpWidth + j];
                        } else if (j < width - (bmpWidth - point_x2)) { // 上边中间部分
                            int index;
                            if (copy_x == 0) {
                                index = point_x1;
                            } else {
                                index = ((j - point_x1) % copy_x) + point_x1;
                            }
                            newPixels[i * width + j] = bmpPixels[i * bmpWidth + index];
                        } else {                        // 右上角区域
                            newPixels[i * width + j] = bmpPixels[i * bmpWidth + j - (width - bmpWidth)];
                        }
                    } else if (i < height - (bmpHeight - point_y2)) {
                        if (j < point_x1) {
                            // 左边中间部分
                            int index;
                            if (copy_y == 0) {
                                index = point_y1;
                            } else {
                                index = point_y1 + ((i - point_y1) % copy_y);
                            }
                            newPixels[i * width + j] = bmpPixels[index * bmpWidth + j];
                        } else if (j < width - (bmpWidth - point_x2)) { // 中间部分
                            int index_X;
                            if (copy_x == 0) {
                                index_X = point_x1;
                            } else {
                                index_X = ((j - point_x1) % copy_x) + point_x1;
                            }
                            int index_Y;
                            if (copy_y == 0) {
                                index_Y = point_y1;
                            } else {
                                index_Y = point_y1 + ((i - point_y1) % copy_y);
                            }
                            newPixels[i * width + j] = bmpPixels[bmpWidth * index_Y + index_X];
                        } else {                         // 右边中间部分
                            int index;
                            if (copy_y == 0) {
                                index = point_y1;
                            } else {
                                index = point_y1 + ((i - point_y1) % copy_y);
                            }
                            newPixels[i * width + j] = bmpPixels[index * bmpWidth + j - (width - bmpWidth)];
                        }
                    } else {
                        int rowNum = i - (height - bmpHeight);
                        if (j < point_x1) {                   // 左下角部分
                            newPixels[i * width + j] = bmpPixels[rowNum * bmpWidth + j];
                        } else if (j < width - (bmpWidth - point_x2)) { // 底部中间部分
                            int index;
                            if (copy_x == 0) {
                                index = point_x1;
                            } else {
                                index = ((j - point_x1) % copy_x) + point_x1;
                            }
                            newPixels[i * width + j] = bmpPixels[rowNum * bmpWidth + index];
                        } else {                        // 右下角部分
                            newPixels[i * width + j] = bmpPixels[rowNum * bmpWidth + j - (width - bmpWidth)];
                        }
                    }
                }
            }
            return Bitmap.createBitmap(newPixels, width, height, Bitmap.Config.ARGB_8888);
        }
    }


}
